# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Services Rest, scaffolding Web

Comme à son habitude, notre meilleur client revient nous voir avec un fort enthousiasme.
Il a eu vent des projets de refonte JSON, et aussi de l'éventuelle création de services Rest. IL n'a vraiment pas tout compris, mais il commence à répendre la nouvelle que notre architecture est déjà 100% Restful
EN plus de ça, il nous demande de prototyper rapidement une interface Web s'appuyant sur nos services...mais donc, il faut les développer de toute urgence !

## Objectifs
* Mises en application :
- [x] **(Exercice 1) Service REST : Lecture d'un Badge**
- [ ] (Exercice 2) Service REST : suppression d'un Badge
- [ ] (Exercice 3) Service REST : Ajout d'un Badge
- [ ] (Exercice 4) Services Rest : Génération d'un client Java et branchement de l'interface Swing en mode client/serveur

----

Donc pour ce premier exercice de la session 5, commençons avec le service REST de lecture de badge

### Gestion des types d'images

 - [ ] Nous allons regarder de plus près dans quel état se trouve la méthode **ResponseEntity<StreamingResponseBody> readBadge(@RequestBody DigitalBadge badge)** dans **BadgesWalletRestServiceImpl**
   
   - [ ] Testez là via l'API, fonctionne-t-elle ? Lors de la restitution du résultat par l'API, avez-vous trouvé le lien de téléchargement de l'image ?
    
   - [ ] Il se trouve que le mimeType est concaténé à la fin du nom du fichier, ôtez cette concaténation, et re-testez. Qu'observez-vous?
    
   - [ ] Par ailleurs nous appliquons un type de Media en retour, via un objet MediaType. Sachant que MediaType étend MimeType, serait-il possible d'instancier uniquement des MediaType dans cette méthode ?
    
   - [ ] Finalement que penseriez-vous de positionner le MediaType obtenu à la place de celui qui est retourné (**OCTET_STREAM**). Re-testez...qu'observez-vous alors ?
   - Bien, votre méthode est simplifiée finalement (regardez bien, c'est possible qu'elle puisse l'être d'avantage...), mais il resterait bien une chose à faire...


 - [ ] A présent, nous allons accéder au code source de MediaType (avec votre IDE) pour identifier les différentessvaleurs possibles correspondant à des images (ex: "image/jpeg" ,...) puis retenir ce formalisme.
    
   - [ ] Il s'agit de faire évoluer notre sérialisation (JSON uniquement) afin de prendre en compte le mimeType
     - [ ] Pour ce faire, aidez-vous de cette classe d'énumération, que vous commenterez au passage (javadoc):
     ```java
      public enum ManagedImages {
        jpeg("image/jpeg"), jpg("image/jpeg"), png("image/png"), gif("image/gif");
        private String mimeType;
        ManagedImages(String mimeType) {
          this.mimeType = mimeType;
        }
        public String getMimeType() {
          return mimeType;
        }
      }
     ```
     - [ ] Servez-vous en (utilisez la méthode valueOf(String)) afin de parser l'extension du nom du fichier de façon à ce qu'elle soit concaténée avec **"image/"**
     - [ ] Ensuite, il n'y a plus grand chose à faire à part inspecter ce qui est déjà là (code fournit avec l'énoncé) et s'intéresser aux tests unitaires...
  
 - [ ] Faites donc évoluer vos tests unitaires pour vérifier la présence d'un mimeType (ajouter au moins une nouvelle assertion),
    
   - _Astuce:_ lancer une première fois, seul, le test **JSONBadgeWalletDAOTest.testAddBadgeOnDatabase** afin d'obtenir le nouveau format de base échantillon, en vérifiant évidemment la présence du mimeType dans le json.
   - [ ] Pensez à trouver une image jpeg afin de faire varier les mimeTypes
    
   - [ ] au passage vous prendrez le wallet.json généré par le test en lieu et place (renommez-le) du db_wallet.json.
  
  
 - [ ] Testez enfin votre API et vérifiez que, quelle que soit l'extension de l'image à récupérer, le lien de téléchargement proposé par l'API est correct, avec une image correctement nommée et exploitable,
  
 - [ ] Testez également à l'aide de la commande curl transmise par l'API

### Gestion du mode de réception du badge

 - [ ] Localisez cette ligne de code dans votre controller Rest:
   - `responseHeaders.add("content-disposition", "attachment; filename=" + fileName)`
   - A votre avis que signifie-t-elle (voici ce que pourrait en dire l'IETF: https://datatracker.ietf.org/doc/html/rfc6266) ?
    
   - [ ] Que se passera-t-il si vous l'enlevez ? Pour de futurs usages, cela serait-il préférable ? 
  
  
 - [ ] AU service de lecture de badges, ajouter un paramètre booléen **attachment**, par défaut à false (aidez-vous de https://www.baeldung.com/spring-request-param, par exemple). Nous souhaitons que par défaut, le header mis en commentaire de soit pas mis.
    
   - [ ] Mais cette fois nous ne voulons pas transmettre ce paramètre dans le json envoyé dans le **RequestBody**, mais plutôt comme paramètre de l'URL d'appel...
  
  
 - [ ] Testez de nouveau notre service (petit conseil, utilisez getMetadata pour récupérer le Json d'un et un seul badge, puis collez le dans l'espace de saisie de la requête au niveau du service readBadge)
   ![](screenshots/attachment.png)
   - [ ] Arrivez-vous à sélectionner simplement le mode de restitution ?
     - [ ] Via l'API
     - [ ] En lignes de commandes
     - Merci por vos chefs d'oeuvre, avec photos/documentation à l'appui ;)




